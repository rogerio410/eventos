package eventos;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import br.edu.ifpi.eventos.model.AtividadeEvento;
import br.edu.ifpi.eventos.model.Cupom;
import br.edu.ifpi.eventos.model.Evento;
import br.edu.ifpi.eventos.model.Inscricao;
import br.edu.ifpi.eventos.model.Pagamento;
import br.edu.ifpi.eventos.model.Profile;
import br.edu.ifpi.eventos.model.TipoAtividadeEvento;
import br.edu.ifpi.eventos.model.Usuario;

public class InscricaoTest {
	
	private Evento evento;
	private Profile joao;
	private Evento eventoComAtividades;
	private Evento eventoComAtividades2;
	private Usuario admin;

	@Before
	public void setUP(){
		
		admin = new Usuario("admin", "admin");
		
		evento = new Evento("ADS Dev Android", 
				"Android do Curso de ADS do IFPI");
		
		eventoComAtividades = new Evento("EventPy", "EventPy Desc");
		AtividadeEvento atividade1 = new AtividadeEvento("Python", TipoAtividadeEvento.MINICURSO);
		atividade1.setValor(new BigDecimal("100.00"));
		eventoComAtividades.addAtividade(atividade1);
		AtividadeEvento atividade2 = new AtividadeEvento("Dj Py", TipoAtividadeEvento.MINICURSO);
		atividade2.setValor(new BigDecimal("100.00"));
		eventoComAtividades.addAtividade(atividade2);
		AtividadeEvento atividade3 = new AtividadeEvento("DevYes", TipoAtividadeEvento.PALESTRA);
		atividade3.setValor(new BigDecimal("100.00"));
		eventoComAtividades.addAtividade(atividade3);
		
		eventoComAtividades2 = new Evento("EventPHP", "EventPHP Desc");
		eventoComAtividades2.addAtividade(new AtividadeEvento("PHP Full", TipoAtividadeEvento.MINICURSO));
		
		joao = new Profile("Rogerio", "rogerio410@", new Usuario("rogerio410", "1234"));
	}

	@Test
	public void Inscricao_recem_criada_deve_ter_zero_atividade(){
		Inscricao inscricao = new Inscricao(joao, evento);
		assertTrue(inscricao.getAtividades().isEmpty());
	}
	
	@Test
	public void Deve_aceitar_incluir_atividades_que_estejam_no_seu_evento(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(0));
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(1));
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(2));
		
		assertEquals(3, inscricao.getAtividades().size());
		for (int i = 0; i < 3; i++) {
			assertEquals(eventoComAtividades.getAtividades().get(i), inscricao.getAtividades().get(i));
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void Nao_deve_aceitar_incluir_atividade_de_outros_evento(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		inscricao.addAtividade(eventoComAtividades2.getAtividades().get(0));
	}
	
	@Test
	public void Nao_deve_incluir_atividades_repetidas(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(0));
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(1));
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(0));
		inscricao.addAtividade(eventoComAtividades.getAtividades().get(1));
		
		assertEquals(2, inscricao.getAtividades().size());
		for (int i = 0; i < 2; i++) {
			assertEquals(eventoComAtividades.getAtividades().get(i), inscricao.getAtividades().get(i));
		}
	}
	
	@Test
	public void Incricao_sem_itens_deve_ter_valor_zero(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		BigDecimal valorZero = new BigDecimal("0.00");
		assertEquals(valorZero, inscricao.totalAPagar());
	}
	
	@Test
	public void Valor_das_inscricao_eh_o_total_dos_seus_itens(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		AtividadeEvento atividade2 = eventoComAtividades.getAtividades().get(1);
		inscricao.addAtividade(atividade1);
		inscricao.addAtividade(atividade2);
		
		assertEquals(new BigDecimal("200.00"), inscricao.totalAPagar());
	}
	
	@Test
	public void Deve_marcar_inscricao_como_paga_ao_receber_pagamentos_do_seu_valor(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		inscricao.addAtividade(atividade1);
		inscricao.addPagamento(new Pagamento(Calendar.getInstance(), admin, "100.00"));
		
		assertTrue(inscricao.isPaga());
	}
	
	@Test
	public void Inscricoes_com_pagamentos_inferiores_ao_valor_a_pagar_devem_estar_como_nao_pagas(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		inscricao.addAtividade(atividade1);
		inscricao.addPagamento(new Pagamento(Calendar.getInstance(), admin, "10.00"));
		inscricao.addPagamento(new Pagamento(Calendar.getInstance(), admin, "30.00"));
		inscricao.addPagamento(new Pagamento(Calendar.getInstance(), admin, "10.00"));
		
		assertFalse(inscricao.isPaga());
	}
	
	@Test(expected=RuntimeException.class)
	public void Inscricao_paga_nao_deve_aceitar_novos_itens(){
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		inscricao.addAtividade(atividade1);
		inscricao.addPagamento(new Pagamento(Calendar.getInstance(), admin, "100.00"));
		
		assertTrue(inscricao.isPaga());
		
		AtividadeEvento atividade2 = eventoComAtividades.getAtividades().get(1);
		//Deve falhar..
		inscricao.addAtividade(atividade2);
	}
	
	@Test
	public void Inscricao_deve_aplicar_descontos_ativos_do_evento(){
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.DAY_OF_YEAR, -10);
		Calendar dataFim = Calendar.getInstance();
		dataFim.add(Calendar.DAY_OF_YEAR, 10);
		Cupom cupom = new Cupom("Teste", dataInicio, dataFim, "30.0");
		
		eventoComAtividades.addCupom(cupom);
		
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		inscricao.addAtividade(atividade1);
		
		assertEquals(new BigDecimal("70.00"), inscricao.totalAPagar());
		
	}
	
	@Test
	public void Nao_deve_aplicar_desconto_de_cupons_nao_ativos(){
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.DAY_OF_YEAR, -30);
		Calendar dataFim = Calendar.getInstance();
		dataFim.add(Calendar.DAY_OF_YEAR, -10);
		Cupom cupom = new Cupom("Teste", dataInicio, dataFim, "30.0");
		
		eventoComAtividades.addCupom(cupom);
		
		Inscricao inscricao = new Inscricao(joao, eventoComAtividades);
		AtividadeEvento atividade1 = eventoComAtividades.getAtividades().get(0);
		inscricao.addAtividade(atividade1);
		
		assertEquals(new BigDecimal("100.00"), inscricao.totalAPagar());
	}
	
}
