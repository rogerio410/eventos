package eventos;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.junit.Test;

import br.edu.ifpi.eventos.model.Cupom;
import br.edu.ifpi.eventos.model.Evento;

public class CupomTest {

	@Test
	public void Deve_ser_ativo_se_estah_no_periodo_de_validade() {
		Calendar dataInicio = Calendar.getInstance();
		dataInicio.add(Calendar.DAY_OF_YEAR, -10);
		Calendar dataFim = Calendar.getInstance();
		dataFim.add(Calendar.DAY_OF_YEAR, 10);
		Cupom cupom = new Cupom("Teste", dataInicio, dataFim, "30.0");
		
		assertTrue(cupom.ativo());
	}
	
	@Test
	public void Nao_deve_ser_Ativo_ser_for_fora_da_validade(){
		Calendar dataInicio = new GregorianCalendar(2016, Calendar.JUNE, 1);
		Calendar dataFim = new GregorianCalendar(2016, Calendar.JUNE, 30);
		Cupom cupom = new Cupom("Teste", dataInicio, dataFim, "30.0");
		
		assertFalse(cupom.ativo());
	}

}
