package eventos;

import org.junit.Before;
import org.junit.Test;

import br.edu.ifpi.eventos.model.AtividadeEvento;
import br.edu.ifpi.eventos.model.EstadoEvento;
import br.edu.ifpi.eventos.model.Evento;
import br.edu.ifpi.eventos.model.Inscricao;
import br.edu.ifpi.eventos.model.Profile;
import br.edu.ifpi.eventos.model.TipoAtividadeEvento;
import br.edu.ifpi.eventos.model.Usuario;

import static junit.framework.Assert.*;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class EventoTest {
	
	private Evento evento;
	private Evento eventoComAtividades;

	@Before
	public void setUP(){
		evento = new Evento("ADS Dev Android", 
				"Android do Curso de ADS do IFPI");
		
		eventoComAtividades = new Evento("EventPy", "EventPy Desc");
		eventoComAtividades.addAtividade(new AtividadeEvento("Python 0", TipoAtividadeEvento.MINICURSO));
		eventoComAtividades.addAtividade(new AtividadeEvento("Dj Py", TipoAtividadeEvento.MINICURSO));
		eventoComAtividades.addAtividade(new AtividadeEvento("DevYes", TipoAtividadeEvento.PALESTRA));
	}
	
	@Test
	public void Deve_Criar_UmEvento_ComNome_EDescricao_NaoPublicado(){
		assertEquals("ADS Dev Android", evento.getNome());
		assertEquals("Android do Curso de ADS do IFPI", evento.getDescricao());
		assertEquals(EstadoEvento.NAO_PUBLICADO, evento.getEstado());
	}
	
	@Test
	public void Evento_recem_criado_deve_ter_zero_atividades(){
		assertTrue(evento.getAtividades().isEmpty());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void Nao_deve_aceitar_eventos_data_passada(){
		Calendar ontem = Calendar.getInstance();
		ontem.add(Calendar.DAY_OF_YEAR, -1);
		evento.setData(ontem);
	}
	
	@Test
	public void Deve_aceitar_eventos_com_data_hoje_ou_futura(){
		Calendar data10DiasFuturo = Calendar.getInstance();
		data10DiasFuturo.add(Calendar.DAY_OF_YEAR, 10);
		evento.setData(data10DiasFuturo);
		
		assertEquals(data10DiasFuturo, evento.getData());
	}
	
	@Test
	public void Deve_settar_automaticamente_em_inscricao_este_evento(){
		Profile rogerio = new Profile("Rogerio", "rogerio410@", new Usuario("rogerio410", "1234"));
		Inscricao inscricao = new Inscricao(rogerio, evento);
		assertTrue(inscricao.getEvento().equals(evento));
	}

}
