package dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import br.edu.ifpi.eventos.dao.GenericDAO;
import br.edu.ifpi.eventos.dao.GenericJPADAO;
import br.edu.ifpi.eventos.dao.UsuarioDAO;
import br.edu.ifpi.eventos.model.Profile;
import br.edu.ifpi.eventos.model.Usuario;

public class GenericJPADAOTest {

	@Test
	public void deve_salva_uma_entidade_simples_e_retornar_seu_id() {
		GenericDAO<Usuario> dao = new GenericJPADAO<Usuario>(Usuario.class);
		Usuario usuario = new Usuario("rogerio410", "1234");
		
		Usuario usuarioSalvo = dao.save(usuario);
		assertTrue(usuarioSalvo.getId() > 0);
	}
	
	@Test
	public void deve_obter_um_objeto_pelo_id(){
		GenericDAO<Usuario> dao = new UsuarioDAO();
		int id = dao.find().get(0).getId();
		assertEquals(id, dao.findByID(id).getId());
	}
	
	@Test
	public void deve_remover_um_objeto(){
		GenericDAO<Usuario> dao = new UsuarioDAO();
		Usuario u = dao.save(new Usuario("teste", "teste"));
		int id = u.getId();
		dao.delete(dao.findByID(id));
	}
	
	@Test
	public void deve_buscar_todos_os_objetos(){
		GenericDAO<Usuario> dao = new GenericJPADAO<Usuario>(Usuario.class);
		List<Usuario> usuarios = dao.find();
		assertTrue(usuarios.size() > 0);
	}
	
	@Test
	public void deve_salvar_entity_e_seu_relacionamento_onetoone_cascaded(){
		GenericDAO<Profile> dao = new GenericJPADAO<Profile>(Profile.class);
		Usuario u = new Usuario("TesteComProfile", "");
		Profile p = new Profile("Rogerio Silva", "rogerio410@", u);
		dao.save(p);
	}
	
	@Test
	public void deve_remover_entity_e_seu_relacionamento_onetoone_cascaded(){
		GenericDAO<Profile> dao = new GenericJPADAO<Profile>(Profile.class);
		Profile p = dao.findByID(3);
		System.out.println(p);
		dao.delete(p);
	}

}
