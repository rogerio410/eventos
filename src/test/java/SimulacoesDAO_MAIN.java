import java.util.List;

import br.edu.ifpi.eventos.dao.GenericDAO;
import br.edu.ifpi.eventos.dao.GenericJPADAO;
import br.edu.ifpi.eventos.model.AtividadeEvento;
import br.edu.ifpi.eventos.model.Evento;
import br.edu.ifpi.eventos.model.Profile;
import br.edu.ifpi.eventos.model.TipoAtividadeEvento;

public class SimulacoesDAO_MAIN {
	
	public static void main(String[] args) {
		
		GenericDAO<Evento> dao = new GenericJPADAO<Evento>(Evento.class);
		
		Evento evento = new Evento("SECA", "Semana do Calouro.");
		evento.addAtividade(new AtividadeEvento("Python 3", TipoAtividadeEvento.PALESTRA));
		evento.addAtividade(new AtividadeEvento("Docker", TipoAtividadeEvento.MINICURSO));
		
		dao.save(evento);
		
		List<Evento> eventos = dao.find();
		
		for (Evento ev : eventos) {
			System.out.println("Size: " + ev.getAtividades().size());
			
		}
		
		
		
		
		
	}

}
