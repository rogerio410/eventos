package br.edu.ifpi.eventos.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Profile {
	
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private String email;
	
	@OneToOne(cascade=CascadeType.ALL)
	private Usuario usuario;
	
	@Transient
	private List<Inscricao> inscricoes;
	
	public Profile() {}

	public Profile(String nome, String email, Usuario usuario) {
		this.nome = nome;
		this.email = email;
		this.usuario = usuario;
	}
	


}
