package br.edu.ifpi.eventos.model;

public enum TipoAtividadeEvento {
	
	PALESTRA, 
	MINICURSO;

}
