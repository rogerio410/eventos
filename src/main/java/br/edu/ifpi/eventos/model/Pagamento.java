package br.edu.ifpi.eventos.model;

import java.math.BigDecimal;
import java.util.Calendar;

public class Pagamento {
	
	private Calendar data;
	private Usuario usuario;
	private BigDecimal valor;
	private Inscricao inscricao;
	
	public Pagamento(Calendar data, Usuario usuario, String valor) {
		this.data = data;
		this.usuario = usuario;
		this.valor = new BigDecimal(valor);
	}

	void setInscricao(Inscricao inscricao) {
		this.inscricao = inscricao;
	}
	
	public BigDecimal getValor() {
		return valor;
	}

}
