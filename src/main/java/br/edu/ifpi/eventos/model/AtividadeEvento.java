package br.edu.ifpi.eventos.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class AtividadeEvento {
	
	@Id
	@GeneratedValue
	private int id;
	private String titulo;
	private String descricao;
	private String responsavel;
	private TipoAtividadeEvento tipo;
	private BigDecimal valor;
	
	@ManyToOne
	private Evento evento;
	
	public AtividadeEvento() {}
	
	public AtividadeEvento(String titulo, TipoAtividadeEvento tipo) {
		this.titulo = titulo;
		this.tipo = tipo;
		this.valor = new BigDecimal("0.00");
	}
	
	public BigDecimal getValor() {
		return valor;
	}
	
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	
	public String toString() {
		return this.titulo + "  R$ "+ this.valor;
	}
	
	public int getId() {
		return id;
	}

	void setEvento(Evento evento) {
		this.evento = evento;
	}
	
}
