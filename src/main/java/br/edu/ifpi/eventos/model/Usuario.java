package br.edu.ifpi.eventos.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Usuario {
	
	@Id
	@GeneratedValue
	private int id;
	private String login;
	private String senha;
	private boolean ativo;
	
	Usuario() {
		// TODO Auto-generated constructor stub
	}
	
	public Usuario(String login, String senha) {
		this.login = login;
		this.senha = senha;
		this.ativo = true;
	}
	
	@Override
	public String toString() {
		return id + " > " + login + " > " + senha;
	}
	
	public int getId() {
		return id;
	}

}
