package br.edu.ifpi.eventos.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

public class Inscricao{
	
	//Amarrar uma inscricao a um evento.
	private Evento evento;
	private Profile profile;
	private List<AtividadeEvento> atividades;
	private boolean paga;
	private List<Pagamento> pagamentos;
	
	public Inscricao(Profile profile, Evento evento) {
		this.paga = false;
		this.profile = profile;
		this.atividades = new ArrayList<AtividadeEvento>();
		this.evento = evento;
		this.evento.addInscricao(this);
		this.pagamentos = new ArrayList<Pagamento>();
	}

	public List<AtividadeEvento> getAtividades() {
		return Collections.unmodifiableList(this.atividades);
	}
	
	public void addAtividade(AtividadeEvento atividade){
		
		if (this.paga) 
			throw new RuntimeException("Inscricao já paga.");
		
		if (this.atividades.contains(atividade))
			return;
		
		if (this.evento.getAtividades().contains(atividade)){
			this.atividades.add(atividade);
		}else{
			throw new IllegalArgumentException("Atividade não pertece ao evento da inscricao");
		}
	}

	public boolean isPaga() {
		return paga;
	}

	public Evento getEvento() {
		return evento;
	}

	public BigDecimal totalAPagar() {
		
		BigDecimal valor = new BigDecimal("0.00");
		for (AtividadeEvento atividadeEvento : atividades) {
			valor = valor.add(atividadeEvento.getValor());
		}
		
		BigDecimal desconto = new BigDecimal("0.00");
		for (Cupom cupom : this.evento.cuponsAtivos()) {
			desconto = cupom.valorDesconto(valor);
		}
		
		return valor.subtract(desconto);
	}
	
	public void addPagamento(Pagamento pagamento) {
		this.pagamentos.add(pagamento);
		pagamento.setInscricao(this);
		marcarComoPaga();
	}
	
	private void marcarComoPaga() {
		if (valorPago().compareTo(totalAPagar()) >= 0){
			this.paga = true;
		}
	}
	
	public BigDecimal valorPago() {
		BigDecimal valor = new BigDecimal("0.00");
		for (Pagamento pagamento : pagamentos) {
			valor = valor.add(pagamento.getValor());
		}
		return valor;
	}
	
}
