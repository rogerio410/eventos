package br.edu.ifpi.eventos.model;

import java.math.BigDecimal;
import java.util.Calendar;

public class Cupom {
	
	private Evento evento;
	private String descricao;
	private Calendar validadeInicio;
	private Calendar validadeFim;
	private BigDecimal percentualDesconto;
	
	public Cupom(String descricao, Calendar validadeInicio, Calendar validadeFim,
			String percentualDesconto) {
		this.descricao = descricao;
		this.validadeInicio = validadeInicio;
		this.validadeFim = validadeFim;
		this.percentualDesconto = new BigDecimal(percentualDesconto);
	}
	
	public BigDecimal getPercentualDesconto() {
		return percentualDesconto;
	}
	
	void setEvento(Evento evento) {
		this.evento = evento;
	}
	
	public boolean ativo(){
		Calendar hoje = Calendar.getInstance();
        if (hoje.compareTo(validadeInicio) >= 0 && hoje.compareTo(validadeFim) <= 0) {
            return true;
        }
        else {
            return false;
        }
	}
	
	public BigDecimal valorDesconto(BigDecimal valorInscricao){
		BigDecimal desconto = new BigDecimal("0.00");
		BigDecimal cem = new BigDecimal("100.00");
		return desconto.add(this.getPercentualDesconto()
				.multiply(valorInscricao)
				.divide(cem));
	}

}
