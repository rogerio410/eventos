package br.edu.ifpi.eventos.model;

public interface Notificador {
	
	//Dependendo de Implementacao (DIP Fail)
	public void enviarNotificacao(String mensagem, Profile destinatario);

}
