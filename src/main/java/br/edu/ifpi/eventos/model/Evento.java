package br.edu.ifpi.eventos.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Evento {
	
	@Id
	@GeneratedValue
	private int id;
	private String nome;
	private String descricao;
	private String slug;
	private Calendar data;

	@OneToMany(mappedBy="evento", cascade=CascadeType.ALL)
	private List<AtividadeEvento> atividades;
	
	@Transient
	private List<Cupom> cupons;
	
	//Representa a se evento está aberto ou nao.
	private EstadoEvento estado;
	
	@Transient
	private List<Inscricao> inscricoes;
	
	//Para o Hibernate.
	Evento(){}
	
	public Evento(String nome, String descricao) {
		this.nome = nome;
		this.descricao = descricao;
		this.estado = EstadoEvento.NAO_PUBLICADO;
		this.atividades = new ArrayList<AtividadeEvento>();
		this.inscricoes = new ArrayList<Inscricao>();
		this.cupons = new ArrayList<Cupom>();
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public EstadoEvento getEstado() {
		return estado;
	}


	public void setEstado(EstadoEvento estado) {
		this.estado = estado;
	}


	public List<AtividadeEvento> getAtividades() {
		return Collections.unmodifiableList(this.atividades);
	}


	public Calendar getData() {
		return data;
	}


	public void setData(Calendar data) throws IllegalArgumentException {
		Calendar hoje = Calendar.getInstance();
		if (data.before(hoje))
			throw new IllegalArgumentException("Nao é possível criar evento com data no passado.");
		this.data = data;
	}

	void addInscricao(Inscricao inscricao) {
		//if (inscricao.getAtividades().isEmpty())
		//	throw new IllegalArgumentException("Inscricao vazia.");
		this.inscricoes.add(inscricao);
	}
	
	public void addAtividade(AtividadeEvento atividade){
		this.atividades.add(atividade);
		atividade.setEvento(this);
	}
	
	public void addCupom(Cupom cupom){
		this.cupons.add(cupom);
		cupom.setEvento(this);
	}
	
	public List<Cupom> cuponsAtivos(){
		List<Cupom> lista = new ArrayList<Cupom>();
		for (Cupom cupom : cupons) {
			if (cupom.ativo()) lista.add(cupom);
		}
		return Collections.unmodifiableList(lista);
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

}
