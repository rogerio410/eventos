package br.edu.ifpi.eventos.model;

public interface Notificavel {
	
	public void setNotificador(Notificador notificardor);
	public void notificar();

}
