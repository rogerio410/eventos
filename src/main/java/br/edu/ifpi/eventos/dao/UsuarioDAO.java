package br.edu.ifpi.eventos.dao;

import br.edu.ifpi.eventos.model.Usuario;

/*
 * Para uso:
 * GenericDAO<Usuario> dao = new UsuarioDAO;
 * inves de: GenericDAO<Usuario> dao = new GenericJPADAO<Usuario>(Usuario.class);
 * 
 * Uso desta classe por enquanto sem sentido.. Tentar deixar tudo no 
 * GenericJPADAO	
 */

public class UsuarioDAO extends GenericJPADAO<Usuario>{
	
	public UsuarioDAO() {
		super(Usuario.class);
	}

}
