package br.edu.ifpi.eventos.dao;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class GenericJPADAO<T> implements GenericDAO<T>{
	
	private EntityManager em;
	private Class<T> persistenceClass;

	@SuppressWarnings("unchecked")
	public GenericJPADAO(Class<T> persistenceClass) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("eventos-local-mysql");
		em = emf.createEntityManager();
		this.persistenceClass = persistenceClass;
		
		//Descobrir qual Class foi passado no "T"
		//Cenario 1(Desativado): GenericDAO<Usuario> dao = new GenericJPADAO<Usuario>();
		
		
		//Cenario 2: GenericDAO<Usuario> dao = new UsuarioDAO(); 
		//Cenario 3: UsuarioDAO dao = new UsuarioDAO();
		//Type sooper = getClass().getGenericSuperclass();
		//Type t = ((ParameterizedType)sooper).getActualTypeArguments()[ 0 ];
		//Para evitar estes cenário torne-me Final. kkkk
		
		//https://github.com/jhalterman/typetools
		//Class<?>[] args = TypeResolver.resolveRawArguments(GenericJPADAO.class, getClass());
		//this.persistenceClass = (Class<T>)args[0];
		
	    //this.persistenceClass = (Class<T>)t;
	}

	public T save(T objeto) {
		
		em.getTransaction().begin();
		T mergedObj = em.merge(objeto);
		em.getTransaction().commit();
		
		return mergedObj;
	}

	public void delete(T objeto) {
		em.getTransaction().begin();
		em.remove(em.merge(objeto));
		em.getTransaction().commit();
	}
	
	public T findByID(Serializable id) {
		T obj = (T)em.find(persistenceClass, id);
		return obj;
	}

	public List<T> find() {
		TypedQuery<T> query = em.createQuery("select c from " + this.persistenceClass.getSimpleName()+" c", this.persistenceClass);
		return query.getResultList();
	}

}
